import csv
from collections import defaultdict
from datetime import datetime
from parser import Parser
from time import time

import requests

HEADER = [
    "category",
    "city",
    "degree",
    "gender",
    "major",
    "number",
    "program_type",
    "school",
    "school_type",
]


class StuParser(Parser):
    def __replace_url(self, key, year):
        new_url = self.url.get(key)
        new_url = new_url.replace("year", str(year))
        print("Download url", new_url)
        return new_url

    def __basicParser(self, url, task):
        start_time = int(time())
        output = []
        with requests.Session() as s:
            resp = s.get(url)
            if resp.status_code != 200:
                raise Exception("Download file fail. the status code", resp.status_code)

            resp = resp.content.decode("utf-8")
            cr = list(csv.reader(resp.splitlines(), delimiter=","))
            temp_header = defaultdict(list)

            for i, attr in enumerate(cr[0]):
                if "名稱" in attr:
                    temp_header[attr].append(i)
                elif "學生數" in attr:
                    if "男" in attr:
                        temp_header["男"].append(i)
                    elif "女" in attr:
                        temp_header["女"].append(i)
            print(temp_header, len(temp_header))

            header = []
            header += temp_header.keys()
            output.append(",".join(header))
            for row in cr[1:]:
                line = []
                for v in temp_header.values():
                    if len(v) > 1:
                        temp = 0
                        for i in v:
                            temp += int(row[i])
                        line.append(str(temp))
                    else:
                        v = v[0]
                        line.append(row[v])
                output.append(",".join(line))

        print(
            "Task end",
            task,
            "running time",
            int(time()) - start_time,
            "len",
            len(output),
        )
        print("\n".join(output[-10:]))

        # 縣市名稱,學校名稱,男,女
        # 新北市,私立淡江高中附設國小,245,243
        # 新北市,私立康橋高中附設國小,866,862

        # 縣市名稱,學校名稱,男,女
        # 新北市,國立華僑中學,16,19
        # 新北市,私立淡江高中附設國中部,451,384
        print("Write to stu/%s.csv" % task)
        self.write_to_s3("stu/%s.csv" % task, "\n".join(output))

    def __highSchoolParser(self, url, degree):
        start_time = int(time())
        output = []
        with requests.Session() as s:
            resp = s.get(url)
            if resp.status_code != 200:
                raise Exception("Download file fail. the status code", resp.status_code)

            resp = resp.content.decode("utf-8")
            cr = list(csv.reader(resp.splitlines(), delimiter=","))
            temp_header = defaultdict(list)

            for i, attr in enumerate(cr[0]):
                if "名稱" in attr:
                    temp_header[attr].append(i)
                elif "學生數" in attr:
                    if "男" in attr:
                        temp_header["男"].append(i)
                    elif "女" in attr:
                        temp_header["女"].append(i)

            print(temp_header, len(temp_header))
            # {'縣市名稱': [1], '學校名稱': [3], '學程名稱': [5], '日夜別名稱': [7],
            # '男': [12, 14, 16, 18, 20], '女': [13, 15, 17, 19, 21]})
            header = ["等級別"]
            header += temp_header.keys()
            output.append(",".join(header))

            for row in cr[1:]:
                line = [degree]
                is_senior = True
                for v in temp_header.values():
                    if len(v) > 1:
                        temp = 0
                        for i in v:
                            temp += int(row[i])
                        line.append(str(temp))
                    else:
                        v = v[0]
                        if row[v] == "附設國中部":
                            is_senior = False
                        line.append(row[v])
                if is_senior:
                    output.append(",".join(line))

            print(
                "Task end",
                degree,
                "running time",
                int(time()) - start_time,
                "len",
                len(cr),
                len(output),
            )

        # 等級別,縣市名稱,學校名稱,學程名稱,日夜別名稱,男,女
        # 高中,新北市,國立華僑高級中等學校,普通科,日間部,44,28
        # 高中,新北市,國立華僑高級中等學校,綜合高中,日間部,652,779
        # 五專,桃園市,新生醫護管理專科學校,護理科原住民專班,日間部,4,40
        # 五專,桃園市,新生醫護管理專科學校,視光學科,日間部,74,221
        # 五專,桃園市,新生醫護管理專科學校,幼兒保育科,日間部,3,292
        # 五專,桃園市,新生醫護管理專科學校,美容造型科,日間部,10,183

        print("\n".join(output[-10:]))
        return output

    def __collageParser(self, url, prev_output):
        start_time = int(time())
        output = []
        with requests.Session() as s:
            resp = s.get(url)
            if resp.status_code != 200:
                raise Exception("Download file fail. the status code", resp.status_code)

            resp = resp.content.decode("utf-8")
            cr = list(csv.reader(resp.splitlines(), delimiter=","))
            temp_header = defaultdict(list)

            for i, attr in enumerate(cr[0]):
                if "代碼" in attr or "計" in attr:
                    continue
                if "男" in attr:
                    temp_header["男"].append(i)
                elif "女" in attr:
                    temp_header["女"].append(i)
                else:
                    temp_header[attr].append(i)

            temp_header = sorted(temp_header.items(), key=lambda x: len(x[1]))
            print(temp_header, len(temp_header))
            header = []
            header += [k for k, v in temp_header]
            output.append(",".join(header))
            # 學校名稱,科系名稱,日間∕進修別,等級別,縣市名稱,體系別,男,女

            for row in cr[1:]:
                line = []
                pre_number = defaultdict(int)
                post_number = defaultdict(int)
                isSpecial = False
                for k, v in temp_header:
                    if len(v) == 1:
                        v = v[0]
                        if "別" in k or "縣市" in k:
                            temp = row[v]
                            temp = temp.split()[1]
                            line.append(temp)
                            if temp == "五專":
                                isSpecial = True
                        else:
                            line.append(row[v])
                    elif len(v) > 1:
                        for i in v[:3]:
                            pre_number[k] += int(row[i]) if row[i].isdigit() else 0
                        for i in v[3:]:
                            post_number[k] += int(row[i]) if row[i].isdigit() else 0
                if isSpecial:
                    line.append(str(post_number["男"]))
                    line.append(str(post_number["女"]))
                    to_pre = [
                        line[3],
                        line[4],
                        line[0],
                        line[1],
                        "日間部" if line[2] == "日" else line[2],
                        str(pre_number["男"]),
                        str(pre_number["女"]),
                    ]
                    prev_output.append(",".join(to_pre))
                else:
                    line.append(str(pre_number["男"] + post_number["男"]))
                    line.append(str(pre_number["女"] + post_number["女"]))
                output.append(",".join(line))

        print(
            "Task collage end",
            "running time",
            int(time()) - start_time,
            "len",
            len(output),
            len(prev_output),
            len(cr),
        )

        print("\n".join(prev_output[-10:]))
        print("Write to stu/senior_high.csv")
        self.write_to_s3("stu/senior_high.csv", "\n".join(prev_output))
        print("\n".join(output[:10]))
        # 學校名稱,科系名稱,日間∕進修別,等級別,縣市名稱,體系別,男,女
        # 國立政治大學,教育學系,日,博士,臺北市,一般,45,56
        # 國立政治大學,教育學系,日,碩士,臺北市,一般,15,24
        # 國立政治大學,教育學系,日,學士,臺北市,一般,78,140
        # 國立政治大學,教育行政與政策研究所,日,碩士,臺北市,一般,14,34
        # 國立政治大學,學校行政碩士在職專班,職,碩士,臺北市,一般,28,52
        print("Write to stu/collage.csv")
        self.write_to_s3("stu/collage.csv", "\n".join(output))

    def parse(self):
        curr_year = datetime.now()
        curr_year = curr_year.year - 1912
        output = []
        self.__basicParser(self.__replace_url("elementary", curr_year), "elementary")
        self.__basicParser(self.__replace_url("joniorHigh", curr_year), "junior_high")
        output = self.__highSchoolParser(
            self.__replace_url("highSchool", curr_year), "高中"
        )
        self.__collageParser(self.__replace_url("collage", curr_year), output)
