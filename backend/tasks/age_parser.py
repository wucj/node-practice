from parser import Parser

import requests
import xlrd


class AgeParser(Parser):
    def parse(self):
        with requests.Session() as s:
            resp = s.get(self.url)
            book = xlrd.open_workbook(file_contents=resp.content)
            sheet = book.sheet_by_index(1)

            row_position = dict()
            header = ["City", "Gender"]
            for col in range(sheet.ncols):
                cell_data = sheet.cell_value(3, col)
                if cell_data:
                    cell_data = cell_data.replace("\n", "").replace("\u3000", "")[:-1]
                    if cell_data.isdigit():
                        row_position[int(cell_data)] = col
                        header.append(cell_data)

            header.append("100")
            row_position[100] = row_position[99] + 1
            output = [",".join(header)]

            row = 0
            while row < sheet.nrows:
                string = sheet.cell_value(row, 0)
                if "縣" in string or "市" in string or "總" in string:
                    if "總" in string:
                        string = "All"
                    else:
                        string = string.split("\n")
                        string = string[0].strip()
                        string = "".join(string.split())
                    for i in range(1, 3):
                        row_data = [string]
                        row_data.append("M" if i == 1 else "F")

                        for k, v in row_position.items():
                            num = str(int(sheet.cell_value(row + i, v)))
                            row_data.append(num)
                        output.append(",".join(row_data))
                    row += 3
                else:
                    row += 1
            print("\n".join(output))
            self.write_to_s3("age.csv", "\n".join(output))
