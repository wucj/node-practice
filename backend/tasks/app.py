import os
import sys

stage = os.getenv("STAGE", "")
if stage == "dev":
    cur_dir = os.path.abspath(os.getcwd())
    sys.path.append(cur_dir + "/python_modules")


import json
import traceback

from age_parser import AgeParser
from diff_parser import DiffParser
from grad_parser import GradParser
from labor_parser import LaborParser
from stu_parser import StuParser

BUCKET_NAME = "public-static-storage"
PREFIX = "csv"

URLS = {
    "grad": {
        "legacy_url": "http://stats.moe.gov.tw/files/main_statistics/u.xls",
        "url": "https://stats.moe.gov.tw/files/detail/year/year_graduatesc.csv",
    },
    "stu": {
        "elementary": "https://stats.moe.gov.tw/files/detail/year/year_basec.csv",
        "joniorHigh": "https://stats.moe.gov.tw/files/detail/year/year_basej.csv",
        "highSchool": "https://stats.moe.gov.tw/files/detail/year/year_base3.csv",
        "collage": "https://stats.moe.gov.tw/files/detail/year/year_students.csv",
    },
}


def lambda_handler(event, context):
    """Sample pure Lambda function

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    ------
    API Gateway Lambda Proxy Output Format: dict

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """

    try:

        query_param = event["queryStringParameters"]
        if "name" not in query_param:
            return {"statusCode": 400, "body": "task name is essential."}

        name = query_param.get("name").lower()
        if name in ["age", "diff", "edu", "mar", "labor"]:
            if "url" not in query_param:
                return {"statusCode": 400, "body": "url is essential."}
            url = query_param.get("url")
            print("Download file from", url)
        else:
            url = URLS.get(name)

        parser_name = name.capitalize() + "Parser"
        print("Get parser", parser_name)
        parser = eval(parser_name)(url, BUCKET_NAME, PREFIX)
        parser.parse()

        return {
            "statusCode": 200,
            "headers": {
                "Access-Control-Allow-Headers": "Content-Type",
                # "Access-Control-Allow-Origin": "https://www.example.com",
                # "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
            },
            "body": json.dumps(
                {
                    "message": "Run task %s successfully." % (parser_name),
                }
            ),
        }

    except:
        # Send some context about this error to Lambda Logs
        traceback.print_exc()
        return {
            "statusCode": 500,
            "body": json.dumps({"message": "Run task %s failed." % (parser_name)}),
        }
