import csv
from collections import defaultdict
from datetime import datetime
from parser import Parser

import requests
import xlrd

to_eng = {"細學類": "major", "男生": "male", "女生": "female", "等級別": "degree"}
rename = {"五專": "副學士", "二專": "副學士"}
classifyMap = {
    "011": "教育學門",
    "021": "藝術學門",
    "022": "人文學門",
    "023": "語文學門",
    "031": "社會及行為科學學門",
    "032": "新聞學及圖書資訊學門",
    "041": "商業及管理學門",
    "042": "法律學門",
    "051": "生命科學學門",
    "052": "環境學門",
    "053": "物理、化學及地球科學學門",
    "054": "數學及統計學門",
    "061": "資訊通訊科技學門",
    "071": "工程及工程業學門",
    "072": "製造及加工學門",
    "073": "建築及營建工程學門",
    "081": "農業學門",
    "082": "林業學門",
    "083": "漁業學門",
    "084": "獸醫學門",
    "091": "醫藥衛生學門",
    "092": "社會福利學門",
    "101": "餐旅及民生服務學門",
    "102": "衛生及職業衛生服務學門",
    "103": "安全服務學門",
    "104": "運輸服務學門",
    "999": "其他學門",
}


class GradParser(Parser):
    def __legacy_parser(self, target_list):
        with requests.Session() as s:
            resp = s.get(self.url.get("legacy_url"))
            book = xlrd.open_workbook(file_contents=resp.content)
            for name in book.sheet_names():
                if "u4" not in name:
                    continue

                year = int(name.split("u")[0])
                if year not in target_list:
                    continue

                year += 1911
                header = ["Year", "Major", "Degree", "Gender", "Number"]
                output = []
                output.append(",".join(header))
                sheet = book.sheet_by_name(name)

                degree_index = {}
                for col in range(2, sheet.ncols):
                    cell_data = sheet.cell_value(3, col)
                    if cell_data:
                        degree_index[(cell_data, "M")] = col
                        degree_index[(cell_data, "F")] = col + 1

                for row in range(6, 33):
                    major = sheet.cell_value(row, 0)
                    if major:
                        if "三分類" in major:
                            break
                        for k, v in degree_index.items():
                            degree, gender = k
                            line_data = [str(year), major]
                            num = int(sheet.cell_value(row, v))
                            if num:
                                line_data.append(degree)
                                line_data.append(gender)
                                line_data.append(str(num))
                                output.append(",".join(line_data))
                print("Write to grad/%s.csv" % year)
                print("\n".join(output))
                self.write_to_s3("grad/%s.csv" % year, "\n".join(output))

    def __new_parser(self, curr_year):
        with requests.Session() as s:
            new_url = self.url.get("url")
            new_url = new_url.replace("year", str(curr_year))
            print("Download url", new_url)
            resp = s.get(new_url)
            if resp.status_code != 200:
                raise Exception("Download file fail. the status code", resp.status_code)

            curr_year += 1911
            header = ["Year", "Major", "Degree", "Gender", "Number"]
            total = defaultdict(int)

            resp = resp.content.decode("utf-8")
            cr = list(csv.reader(resp.splitlines(), delimiter=","))

            for row in cr[1:]:
                major = row[0][:-2]
                major = classifyMap[major]

                degree = row[4].split()
                degree = degree[1]
                if degree in rename:
                    degree = rename[degree]

                male = row[5].replace(",", "")
                if male.isdigit():
                    male = int(male)
                else:
                    male = 0

                female = row[6].replace(",", "")
                if female.isdigit():
                    female = int(female)
                else:
                    female = 0

                total[(major, degree, "M")] += male
                total[(major, degree, "F")] += female

            output = []
            output.append(",".join(header))
            for k, v in total.items():
                line = [str(curr_year), k[0], k[1], k[2], str(v)]
                output.append(",".join(line))
            print("\n".join(output))
            print("Write to grad/%s.csv" % curr_year)
            self.write_to_s3("grad/%s.csv" % curr_year, "\n".join(output))

    def parse(self):
        curr_year = datetime.now()
        target_list = [y - 1912 for y in range(curr_year.year - 7, curr_year.year + 1)]
        self.__legacy_parser(target_list)
        target_list = [y for y in target_list if y >= 110]
        for y in target_list:
            self.__new_parser(y)
