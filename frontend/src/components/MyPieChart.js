import React, { useState } from 'react';
import { PieChart, Pie, Cell, Tooltip, ResponsiveContainer } from 'recharts';
import MyTable from './MyTable';


const MyPieChart = ({ data, title, tableData }) => {

    const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042', "#8884d8", "#82ca9d", "#FFBB28", "#FF8042", "#AF19FF", "#F870A2"]
    const [showDetail, setShowDetail] = useState(false)

    console.log('piechart', title, data, tableData)
    return (
        <div className="card border-success mb-2">
            <div className="card-header text-bg-success">
                {title}
            </div>
            <div className="card-body">
                <ResponsiveContainer width="100%" height={200}>
                    <PieChart width={300} height={200}>
                        <Pie
                            dataKey="value"
                            isAnimationActive={false}
                            startAngle={180}
                            endAngle={0}
                            data={data}
                            cx="50%"
                            cy="80%"
                            outerRadius={80}
                            innerRadius={20}
                            paddingAngle={2}
                            fill="#8884d8"
                            label
                        >
                            {data.map((entry, index) => (
                                <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                            ))}
                        </Pie>
                        {/* <Pie dataKey="value" data={data02} cx={500} cy={200} innerRadius={40} outerRadius={80} fill="#82ca9d" /> */}
                        <Tooltip />
                    </PieChart>
                </ResponsiveContainer>
                <div className="form-check form-switch my-1">
                    <input className="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckDefault" onChange={() => setShowDetail(!showDetail)} />
                    <label className="form-check-label" for="flexSwitchCheckDefault">Show detail</label>
                </div>
                {showDetail && <MyTable data={tableData} />}
            </div>
        </div>
    );

}
export default MyPieChart;
