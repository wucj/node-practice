from io import BytesIO

import boto3


class Parser:
    def __init__(self, url, bucket_name, prefix):
        self.url = url
        self.bucket_name = bucket_name
        self.prefix = prefix

    def write_to_s3(self, filename, content):
        s3 = boto3.client("s3")
        s3.upload_fileobj(
            BytesIO(content.encode("utf-8")),
            self.bucket_name,
            self.prefix + "/" + filename,
        )

    def parse(self):
        return
