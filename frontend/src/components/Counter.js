import { useSelector, useDispatch } from "react-redux";
import { increment, decrement } from '../features/counterSlice';
import React, {useEffect} from 'react';
import { fetchUsers } from "../features/counterSlice";

const Counter = () => {
    

    // const count = useSelector((state) => state.counter.count);
    const count = useSelector((state) => state.counter);
    console.log('Counter', count)
    const dispatch = useDispatch(); 
    useEffect(() => {
        dispatch(fetchUsers())
    }, [])
    return (
        <section>
            <p>{count.count}</p>
            <div>
                <button onClick={() => dispatch(increment())}>+</button>
                <button onClick={() => dispatch(decrement())}>-</button>
            </div>
            {count.loading && <div>Loading...</div>}
            {!count.loading && count.error && <div>{count.error}</div>}
            {!count.loading && count.users && <ul>
                {count.users.map(user => ( 
                    <li>{user.Year}</li>)
                )}
            </ul>

            }
        </section>
    )
}

export default Counter;