from parser import Parser

import requests
import xlrd


class DiffParser(Parser):
    def parse(self):
        with requests.Session() as s:
            resp = s.get(self.url)
            book = xlrd.open_workbook(file_contents=resp.content)
            sheet_names = book.sheet_names()
            years_data = 7
            inc_header = ["Year", "City", "出生", "死亡", "自然增加", "社會增加", "總增加"]
            mar_header = ["Year", "City", "結婚", "離婚"]
            inc_row_position = {
                "Birth": 1,
                "Death": 5,
                "NaturalInc": 9,
                "SocialInc": 11,
                "TotalInc": 13,
            }
            mar_row_position = {
                "Marring": 15,
                "Divorce": 17,
            }
            inc_output = [",".join(inc_header)]
            mar_output = [",".join(mar_header)]
            for name in sheet_names[1 : 1 + years_data]:
                sheet = book.sheet_by_name(name)

                row = 0
                while row < sheet.nrows:
                    string = sheet.cell_value(row, 0)
                    if "縣" in string or "市" in string or "總" in string:
                        if "總" in string:
                            string = "All"
                        else:
                            string = string.strip()
                            string = "".join(string.split())
                            string = string[:3]

                        inc_row_data = [name, string]
                        mar_row_data = [name, string]

                        total = 0  # filter out the invalid data
                        for k, v in inc_row_position.items():
                            num = int(sheet.cell_value(row, v))
                            total += num
                            inc_row_data.append(str(num))
                        for k, v in mar_row_position.items():
                            num = int(sheet.cell_value(row, v))
                            total += num
                            mar_row_data.append(str(num))
                        if total != 0:
                            inc_output.append(",".join(inc_row_data))
                            mar_output.append(",".join(mar_row_data))

                    row += 1
            print("\n".join(inc_output))
            print("\n".join(mar_output))

            self.write_to_s3("diff_inc.csv", "\n".join(inc_output))
            self.write_to_s3("diff_mar.csv", "\n".join(mar_output))
