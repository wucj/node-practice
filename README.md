# infographic of population in taiwan project
 In v2, I rewrite this project, because heroku doesn't support free hosting service anymore. Hence, I change the whole architecture of this website. Instead of using NEXT.js and hosting server on nodejs, this time, I use python as my backend language and run those python code on AWS lambda function, then instead of store all data on the Google datastore, I choose S3 as my storage, hance, the whole data structure are changed. After parse the original data, I store essential data in the CSV file and use S3 select to retrieve data according to API query. For the frontend side, I create single page application and host it on the S3, due single page application, the page routing is disable at current monent. 

## Development Environment Requirements
docker
aws-cli
sam


## Run develop environment
Currently, the backend side (lambda with SAM) is still a little complicated. For my instance, I choose python3.9 as my target running environment. However, I local python version is 3.6, and seems sam local command will pull docker and run code inside the container, however, we have to handle package installation by ourselves. Hence, I build package and store them on the python_modules diectory, and add them to python path before the import code. I also tried the sam build command, and sam build command can recognize the requirements.txt file and build the target locally for me, but it seems cannot reflect any changes if I don't want to do sam build again. 

    1. Bring up the backend locally 
    ```
    $ cd backend
    $ sam local start-api --env-vars env.json --host 0.0.0.0
    ```
    2. Run the docker for frontend
    ```
    $ cd frontend
    $ docker run --rm -it -v ${PWD}:/app -p 8000:8000 --add-host="host.docker.internal:host-gateway" node bash
    $ cd app
    $ npm start
    ```
    3. Commit change to bitbucket


## Goal of this project
I create this project is for my practice of react/redux. In addition, I am
interested in to analysis some numbers from any kinds of data.
So, I found out that Taiwan Goverment has opened some data related to populations.

Hence, I download those data and show them via several aspect of population.

So, you can explore this site [here](https://tw-population.click/)

Any suggestions, you could file the issue in this repository. Thanks.

