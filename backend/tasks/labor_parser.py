from collections import defaultdict
from datetime import datetime
from parser import Parser

import requests
import xlrd

url_mapping = {
    "gender": {
        "employed": "table08.XLS",
        "unemploye": "table14.XLS",
        "nonLabor": "table18.XLS",
    },
    "age": {
        "employed": "table09.XLS",
        "unemployed": "table15.XLS",
        "nonLabor": "table19.XLS",
    },
    "nonLabor_reason": "table21.XLS",
    "employed_occupation": "table11_8.XLS",
    "employed_role": "table13.XLS",
}


class LaborParser(Parser):
    def __gender_parser(self, cur_year, paths):
        to_csv = []
        header = ["Month", "Status", "Total", "Male", "Female"]
        to_csv.append(",".join(header))
        for label, url in paths.items():
            with requests.Session() as s:
                year_scope = cur_year - 1912
                url = self.url + "/" + url
                print(label, "Download", url)
                resp = s.get(url)
                book = xlrd.open_workbook(file_contents=resp.content)
                sheet = book.sheet_by_index(0)
                output = []
                for r in range(4, sheet.nrows):
                    line = []
                    c = 0
                    while c < 4:
                        cell_data = sheet.cell_value(r, c)
                        if cell_data and c == 0:
                            cell_data = cell_data.strip()
                            if cell_data[-1] == "均":
                                break
                            cell_data = cell_data.replace("年", "")
                            cell_data = cell_data.replace("月", "")
                            cell_data = cell_data.split()
                            if (
                                cell_data[0].isdigit()
                                and int(cell_data[0]) < year_scope
                            ):
                                break
                            new_date = datetime(
                                int(cell_data[0]) + 1911, int(cell_data[1]), 1
                            )
                            line.append(new_date.strftime("%d/%m/%y"))
                            line.append(label)
                        elif cell_data:
                            line.append(int(cell_data) * 1000)
                        c += 1
                    if line:
                        output.append(",".join([str(x) for x in line]))
                output = output[-10:] if len(output) > 10 else output
                to_csv += output
        print("\n".join(to_csv))
        self.write_to_s3("labor/gender.csv", "\n".join(to_csv))

    def __age_parser(self, cur_year, paths):
        to_csv = []
        header = [
            "Month",
            "Status",
        ]
        for n in range(15, 70, 5):
            header.append(str(n))
        to_csv.append(",".join(header))
        for label, url in paths.items():
            with requests.Session() as s:
                year_scope = cur_year - 1912
                url = self.url + "/" + url
                print(label, "Download", url)
                resp = s.get(url)
                book = xlrd.open_workbook(file_contents=resp.content)
                sheet = book.sheet_by_index(0)
                output = []
                for r in range(4, sheet.nrows):
                    line = []
                    c = 0
                    while c < sheet.ncols:
                        cell_data = sheet.cell_value(r, c)
                        if cell_data and c == 0:
                            cell_data = cell_data.strip()
                            if cell_data[-1] == "均":
                                break
                            cell_data = cell_data.replace("年", "")
                            cell_data = cell_data.replace("月", "")
                            cell_data = cell_data.split()
                            if (
                                cell_data[0].isdigit()
                                and int(cell_data[0]) < year_scope
                            ):
                                break
                            new_date = datetime(
                                int(cell_data[0]) + 1911, int(cell_data[1]), 1
                            )
                            line.append(new_date.strftime("%d/%m/%y"))
                            line.append(label)
                            c += 1  # skip column 1
                        elif cell_data:
                            line.append(int(cell_data) * 1000)
                        c += 1
                    if line:
                        output.append(",".join([str(x) for x in line]))
                output = output[-1:] if len(output) > 1 else output
                to_csv += output
        print("\n".join(to_csv))
        self.write_to_s3("labor/age.csv", "\n".join(to_csv))

    def __find_title(self, sheet):
        start_line, end_line = 0, 0
        for r in range(10):
            cell_data = sheet.cell_value(r, 0)
            cell_data = "".join(cell_data.split())
            if cell_data:
                if cell_data == "年月別":
                    start_line = r
                elif "年" in cell_data and "月" in cell_data or "均" in cell_data:
                    end_line = r
                    break
        return start_line, end_line

    def __check_date_format(self, data):
        if "年" in data and "月" in data:
            if "均" in data:
                return False
            else:
                return True
        return False

    def __general_parser(self, cur_year, path, label):

        to_csv = []
        header = []
        with requests.Session() as s:
            year_scope = cur_year - 1912
            url = self.url + "/" + path
            print(label, "Download", url)
            resp = s.get(url)
            book = xlrd.open_workbook(file_contents=resp.content)
            sheet = book.sheet_by_index(0)
            title_start, title_end = self.__find_title(sheet)
            if label == "role":
                title_start -= 1
            print("Title start", title_start, title_end)
            fields = defaultdict(int)
            for c in range(1, sheet.ncols):
                field = ""
                for r in range(title_start, title_end):
                    cell_data = sheet.cell_value(r, c)
                    cell_data = "".join(cell_data.split())
                    if cell_data:
                        field += cell_data
                if field and not ("總計" in field or "合計" in field):
                    fields[field] = c
            fields["Month"] = 0
            fields = sorted(fields.items(), key=lambda x: x[1])
            print(fields)
            for name, c in fields:
                header.append(name)
            to_csv.append(",".join(header))
            output = []
            for r in range(title_end, sheet.nrows):
                line = []
                for name, c in fields:
                    cell_data = sheet.cell_value(r, c)
                    if cell_data and c == 0:
                        if not self.__check_date_format(cell_data):
                            break
                        cell_data = cell_data.strip()
                        if cell_data[-1] == "均":
                            break
                        cell_data = cell_data.replace("年", "")
                        cell_data = cell_data.replace("月", "")
                        cell_data = cell_data.split()
                        if cell_data[0].isdigit() and int(cell_data[0]) < year_scope:
                            break
                        if cell_data[0].isdigit() and cell_data[1].isdigit():
                            new_date = datetime(
                                int(cell_data[0]) + 1911, int(cell_data[1]), 1
                            )
                            line.append(new_date.strftime("%d/%m/%y"))
                    elif cell_data:
                        line.append(int(cell_data) * 1000)
                if line:
                    output.append(",".join([str(x) for x in line]))
            output = output[-1:]
            to_csv += output
        print("\n".join(to_csv))
        self.write_to_s3("labor/%s.csv" % label, "\n".join(to_csv))

    def parse(self):
        # url = "https://ws.dgbas.gov.tw/001/Upload/463/relfile/11039/229924/table05.xls"
        cur_year = datetime.now()
        self.__general_parser(
            cur_year.year, url_mapping.get("nonLabor_reason"), "nonlabor"
        )
        self.__general_parser(
            cur_year.year,
            url_mapping.get("employed_occupation"),
            "occupation",
        )
        self.__general_parser(cur_year.year, url_mapping.get("employed_role"), "role")
        self.__gender_parser(cur_year.year, url_mapping.get("gender"))
        self.__age_parser(cur_year.year, url_mapping.get("age"))
