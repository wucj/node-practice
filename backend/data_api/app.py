import os
import sys

ALLOW_ORIGIN = "https://tw-population.click"
stage = os.getenv("STAGE", "")
if stage == "dev":
    cur_dir = os.path.abspath(os.getcwd())
    sys.path.append(cur_dir + "/python_modules")
    ALLOW_ORIGIN = "*"


import json
import traceback

import boto3

BUCKET_NAME = "public-static-storage"
PREFIX = "csv"


def lambda_handler(event, context):
    """Sample pure Lambda function

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    ------
    API Gateway Lambda Proxy Output Format: dict

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """

    s3 = boto3.client("s3")

    def query_data_from_s3(key_name, query_string, header_info):

        resp = s3.select_object_content(
            Bucket=BUCKET_NAME,
            Key=PREFIX + "/" + key_name,
            ExpressionType="SQL",
            Expression=query_string,
            InputSerialization={
                "CSV": {"FileHeaderInfo": header_info},
                "CompressionType": "NONE",
            },
            OutputSerialization={"CSV": {}},
        )

        buffer = b""
        for event in resp["Payload"]:
            if "Records" in event:
                buffer += event["Records"]["Payload"]

        return buffer.decode("utf-8").split("\n")

    def query_header(key_name):

        header = query_data_from_s3(
            key_name, "select s.* from S3Object s limit 1", "NONE"
        )
        return header[0].split(",")

    def get_key_name(query_type):
        objects = s3.list_objects(
            Bucket=BUCKET_NAME, Prefix=PREFIX + "/" + query_type
        ).get("Contents", [])
        return [obj["Key"] for obj in objects]

    try:
        query_param = event["queryStringParameters"]

        if "type" not in query_param:
            return {"statusCode": 400, "body": "data type is essential."}

        query_type = query_param.get("type").lower()

        if not query_type:
            return {"statusCode": 400, "body": "data type should not empty."}

        if query_type in ["age", "diff_inc", "diff_mar", "stu"] and (
            "filter_key" in query_param
            or "filter_value" in query_param
            or "group_by" in query_param
        ):
            # select part of data from csv file
            if "sub_key" in query_param:
                key_name = (
                    query_param.get("type").lower()
                    + "/"
                    + query_param.get("sub_key")
                    + ".csv"
                )
            else:
                key_name = query_param.get("type").lower() + ".csv"

            if "filter_key" in query_param or "filter_value" in query_param:

                key = query_param.get("filter_key").capitalize()
                value = query_param.get("filter_value")

                header = query_header(key_name)

                data = query_data_from_s3(
                    key_name,
                    "SELECT * FROM s3object s where s.\"%s\" = '%s'" % (key, value),
                    "USE",
                )

            elif "group_by" in query_param:
                value = query_param.get("group_by")

                data = query_data_from_s3(
                    key_name, "SELECT %s FROM s3object" % value, "USE"
                )
                data = list(set(data))
                header = [value]

        elif query_type in ["grad"]:
            data = []
            all_keys = get_key_name(query_type)
            for key_name in all_keys:
                key_name = key_name[4:]
                data += query_data_from_s3(key_name, "SELECT * FROM s3object", "USE")
            header = query_header(all_keys[0][4:])
        elif query_type in ["labor", "stu"]:
            if "sub_key" in query_param:
                data = []
                sub_key = query_param.get("sub_key").lower()
                key_name = query_param.get("type").lower() + "/" + sub_key + ".csv"
                data = query_data_from_s3(key_name, "SELECT * FROM s3object", "USE")
                header = query_header(key_name)
            else:
                return {
                    "statusCode": 400,
                    "body": "filter key and value are essential.",
                }
        else:
            return {
                "statusCode": 400,
                "body": "Incorrect query conditions essential.",
            }

        output = []

        for line in data:
            if line:
                line_data = line.split(",")
                temp = {}
                for i in range(len(line_data)):
                    temp[header[i]] = line_data[i]
                output.append(temp)

        return {
            "statusCode": 200,
            "headers": {
                "Access-Control-Allow-Headers": "Content-Type",
                "Access-Control-Allow-Origin": ALLOW_ORIGIN,
                "Access-Control-Allow-Methods": "OPTIONS,POST,GET",
            },
            "body": json.dumps(output, ensure_ascii=False).encode("utf8"),
        }

    except:
        # Send some context about this error to Lambda Logs
        traceback.print_exc()

        return {"statusCode": 500, "body": "Error"}
